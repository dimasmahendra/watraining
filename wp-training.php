<?php
/*
Plugin Name: Example Testimonial Form Plugin
Description: Simple WordPress Testimonial Form
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
global $wpdb;
function html_form_code() {
  echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
  echo '<p>';
  echo 'Your Name (required) <br/>';
  echo '<input type="text" name="cf-name" pattern="[a-zA-Z0-9 ]+" value="' . ( isset( $_POST["cf-name"] ) ? esc_attr( $_POST["cf-name"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Your Email (required) <br/>';
  echo '<input type="email" name="cf-email" value="' . ( isset( $_POST["cf-email"] ) ? esc_attr( $_POST["cf-email"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Phone Number (required) <br/>';
  echo '<input type="text" name="cf-phonenumber" pattern="[0-9 ]+" value="' . ( isset( $_POST["cf-phonenumber"] ) ? esc_attr( $_POST["cf-phonenumber"] ) : '' ) . '" size="40" />';
  echo '</p>';
  echo '<p>';
  echo 'Your Message (required) <br/>';
  echo '<textarea rows="10" cols="35" name="cf-testimonial">' . ( isset( $_POST["cf-testimonial"] ) ? esc_attr( $_POST["cf-testimonial"] ) : '' ) . '</textarea>';
  echo '</p>';
  echo '<p><input type="submit" name="cf-submitted" value="Send"></p>';
  echo '</form>';
}

function deliver_testimonial() {
    global $wpdb;
    if ( isset( $_POST['cf-submitted'] ) ) {
      $insert = array(
        'name' => $_POST['cf-name'], 
        'email' => $_POST['cf-email'],
        'phone_number' => $_POST['cf-phonenumber'],
        'testimonial' => $_POST['cf-testimonial']
        );
      $type = array( '%s', '%s', '%s', '%s' );
        if ( $wpdb->insert('wp_latihanusertestimonial', $insert, $type) ) {
            echo '<div>';
            echo '<p>Thanks for contacting me, expect a response soon.</p>';
            echo '</div>';
        } else {
            echo 'An unexpected error occurred';
        }
    }
}

function cf_shortcode() {
  ob_start();
  deliver_testimonial();
  html_form_code();
  return ob_get_clean();
}
add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );

/*
Plugin Name: My Admin Page
Description: Simple Admin Page For Tetimonial
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
  ob_start();
  delete_testimonial();
  add_menu_page( 'Testimonials', 'Testimonials', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6  );  
  return ob_get_clean();
}

function delete_testimonial() {
  global $wpdb;
  if ( isset( $_POST['cf-submitted'] ) ) {
    $wpdb->delete( 'wp_latihanusertestimonial', array( 'id' => $_POST['id'] ), array( '%d' ) );
  }
}

function myplguin_admin_page(){
  global $wpdb;
    ?>
    <div class="wrap">
      <h2>Testimonials</h2>
        <?php 
          $testi = $wpdb->get_results( "SELECT * FROM wp_latihanusertestimonial", ARRAY_A);
        ?>
        <table id="tabelTesti" class="wp-list-table widefat fixed striped pages" cellspacing="0" border="1" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Id</th>  
                <th>Nama</th>  
                <th>Email</th>  
                <th>Telepon</th>  
                <th width="30%">Testimonial</th>        
                <th>Edit</th>         
              </tr>
            </thead>
            <tbody>
                <?php
                  $no = 1;            
                  foreach ($testi as $row)
                  { ?>
                        <tr align="center">
                            <td><?php echo $no;$no++; ?></td>
                            <td><?php echo $row['id']; ?></td> 
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $row['phone_number']; ?></td>
                            <td><?php echo $row['testimonial']; ?></td>
                            <td>
                              <form action="#" method="post"><a onclick="return confirm('Apakah Anda yakin ingin menghapus Produk ini?');">
                                <input type="hidden" name="id" id="id" value="<?php echo $row['id']; ?>">
                                <button type="submit" name="cf-submitted" value="Send">Hapus</button> 
                              </a></form>
                            </td>
                        </tr>
                <?php } ?>            
            </tbody>              
        </table>
    </div>
    <?php
}

/*
Widget Name: Widget Testimonial Random
Description: Widget for random Testimonial
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
class wpb_widget extends WP_Widget {

function __construct() {
  parent::__construct('wpb_widget',__('WPBeginner Widget', 'wpb_widget_domain'),
  array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
  );
}

public function widget( $args, $instance ) {
  global $wpdb;
  $title = apply_filters( 'widget_title', $instance['title'] );
  echo $args['before_widget'];  
  $data = $wpdb->get_results( "SELECT * FROM wp_latihanusertestimonial ORDER BY RAND() LIMIT 0,1", OBJECT );
  foreach ($data as $key => $value) {
    $result = "
      <b>Nama :</b> $value->name</br>
      <b>Email :</b> $value->email</br>
      <b>Phone :</b>$value->phone_number</br>
      <b>Testimonial :</b> $value->testimonial</br>
    ";  
  }
  if ( ! empty( $title ) )
  echo $args['before_title'] . $title . $args['after_title'];
  echo __( $result, 'wpb_widget_domain' );
  echo $args['after_widget'];
}
    
public function form( $instance ) {
  if ( isset( $instance[ 'title' ] ) ) {
  $title = $instance[ 'title' ];
  }
  else {
  $title = __( 'New title', 'wpb_widget_domain' );
  }
  ?>
  <p>
  <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
  </p>
  <?php 
}

public function update( $new_instance, $old_instance ) {
  $instance = array();
  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
  return $instance;
  }
}
?>